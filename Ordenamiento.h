#include <iostream>
using namespace std;

#ifndef ORDENAMIENTO_H
#define ORDENAMIENTO_H

class Ordenamiento {
    private:
    public:
        // constructor 
        Ordenamiento();
        // Funcion del metodo prueba lineal
        void pruebalineal (int *arreglo, int numero);
        // Funcion del metodo prueba cuadratica
        void pruebacuadratica(int *arreglo, int numero);
        // Funcion del metodo doble dirrecion hash
        void dobledirrecionhash (int *arreglo, int numero);
        // Funcion hash
        void add_hash (int *arreglo, int numero, char letra);
};
#endif
