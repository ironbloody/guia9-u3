#include <iostream>
#include "Ordenamiento.h"
using namespace std;

Ordenamiento::Ordenamiento() {}

// Funcion metodo prueba lineal
void Ordenamiento::pruebalineal (int *arreglo, int numero){
    // Creacion de variables
    int d, dx;
    
    // Funcion hash con modulo 20
    d = ((numero)%20);
    // Si la posicion d no esta vacia y es igual al numero
    if ((arreglo[d] != '\0') && (arreglo[d] == numero)){
        // Se imprime el lugar de la posicion
        cout << "La informacion esta en la posicion " << d << endl;
    }
    // Sino
    else{
        // dx se iguala a d mas uno
        dx = d + 1;
        /*y mientras dx sea menor o igual a 20, la posicion dx no sea vacia, 
        la posicion dx distinta de numero y dx distinto de d*/
        while ((dx <= 20) && (arreglo[dx] != '\0') && (arreglo[dx] != numero) && (dx != d)){
            // dx se iguala a dx mas uno
            dx = dx + 1;
            // Si dx es igual a 21
            if (dx == 21){
                // dx se iguala a 1
                dx = 1;
            }
        }
        // la posicion dx se iguala a numero
        arreglo[dx] = numero;
        // Si la posicion dx esta vacia o dx es igual a d
        if ((arreglo[dx] == '\0') || (dx == d)){
            cout << "La informacion no se encuentra en el arreglo" << endl;
        }
        else{
            cout << "La informacion esta en la posicion " << dx << endl;
        }
    }
}

// Funcion con el metodo prueba cuadratica
void Ordenamiento::pruebacuadratica(int *arreglo, int numero){
    // Creacion de variables
    int d, dx, i;
    
    // Funcion hash
    d = ((numero)%20);
    // Si la poscion d no esta vacia y es igual a numero
    if ((arreglo[d] != '\0') && arreglo[d] == numero){
        // Se imprime la posicion
        cout << "La informacion esta en la posicion " << d << endl;
    }
    // Sino
    else{
        // Funcion hash alternativa
        i = 1;
        dx = (d +(i * i));
        
        // Mientras que la posicion dx no este vacia y sea diferente de numero
        while ((arreglo[dx] != '\0') && (arreglo[dx] != numero)){
            // Se le sumara uno a i y se realiza hash
            i = i + 1;
            dx = (d+(i * i));
            
            // Si dx es mayor a 20
            if(dx > 20){
                // Se iguala i a 0
                i = 0;
                // Se iguala dx a 1
                dx = 1;
                // Se iguala d a 1
                d = 1;
            }
        }
        // Se iguala la posicion dx a numero
        arreglo[dx] = numero;
        // Si la posicion dx esta vacio
        if (arreglo[dx] == '\0'){
            cout << "La informacion no se encuentra en el arreglo" << endl;
        }
        else{
            cout << "La informacion esta en la posicion " << dx << endl;  
        }
    }
}

void Ordenamiento::dobledirrecionhash (int *arreglo, int numero){
    // Creacion de variables
    int d, dx, i;
    
    // Funcion hash
    d = ((numero)%20);
    // Si la posicion d no esta vacia y es distinta de numero
    if ((arreglo[d] != '\0') && (arreglo[d] == numero)){
        // Se imprime la posicion
        cout << "La informacion esta en la posicion " << d << endl;
    }
    // Sino
    else {
        // Hash alternativa
        dx = ((d+1)%20);
        /* Mientras dx sea menor o igual a 20, posicion dx no este vacia, 
        posicion dx sea distinta de numero y dx sea distinto de d*/
        while ((dx <= 20) && (arreglo[dx] != '\0') && (arreglo[dx] != numero) && (dx != d)){
            // Hash alternativa
            dx = ((dx+1)%20); 
        }
        // la posicion dx se iguala a numero
        arreglo[dx] = numero;
        // Si la posicion dx esta vacia o es distinta de numero
        if ((arreglo[dx] == '\0') || (arreglo[dx] != numero)){
            cout << "La informacion no se encuentra en el arreglo" << endl;
        }
        // sino
        else {
            cout << "La informacion esta en la posicion " << dx << endl;
        }
    }
}

// Funcion Hash
void Ordenamiento::add_hash (int *arreglo, int numero, char letra){
    // Creacion de variables
    int l;
    
    // Si el parametro de entrada es L
    if (letra == 'L'){
        // Hash
        l = ((numero)%20);
        // Si la poscicion l no es nula y es distinta de numero
        if ((arreglo[l] != '\0') && (arreglo[l] != numero)){
            // Sucede colicion
            cout << "Colision" << endl;
            // Se llama al metodo prueba lienal
            Ordenamiento::pruebalineal(arreglo, numero);
        }
        // Sino
        else {
            // La posicion l se iguala a numero
            arreglo[l] = numero; 
            // Se imprime la posicion
            cout << "La informacion esta en la posicion " << l << endl;
        }
    }
    // Si el parametro de entrada es C
    if (letra == 'C'){
        // Hash
        l = ((numero)%20);
        // Si la poscicion l no es nula y es distinta de numero
        if ((arreglo[l] != '\0') && arreglo[l] != numero){
            // Sucede colicion
            cout << "Colision" << endl;
            // Se llama al metodo prueba cuadratica
            Ordenamiento::pruebacuadratica(arreglo, numero);
        }
        // Sino
        else {
            // La posicion l se iguala a numero
            arreglo[l] = numero; 
            // Se imprime la posicion
            cout << "La informacion esta en la posicion " << l << endl;
        }
            
    }
    // Si el parametro de entrada es C
    if (letra == 'D'){
        // Hash
        l = ((numero)%20);
        // Si la poscicion l no es nula y es distinta de numero
        if ((arreglo[l] != '\0') && arreglo[l] != numero){
            // Sucede colicion
            cout << "Colision" << endl;
            // Se llama al metodo doble direccion hash
            Ordenamiento::dobledirrecionhash(arreglo, numero);
        }
        // Sino
        else {
            // La posicion l se iguala al numero
            arreglo[l] = numero; 
            // Se imprime la posicion
            cout << "La informacion esta en la posicion " << l << endl;
        }
    }   
}
