#include <iostream>
using namespace std;
// Clases 
#include "Ordenamiento.h"

// Funcion busqueda
void busqueda(int *arreglo, int numerobuscar){
    // Inicializar contador
    int contador = 0;
    for (int i=0; i<20; i++){
        // Si el numero se encuentra en la posicion
        if (arreglo[i] == numerobuscar){
            cout << "El numero esta en la posicion " << i << endl;
            // El contador aumenta
            contador++;
        }
       
    }
    // Si el contador es 0
    if (contador == 0){
        cout << "El numero no se encuentra en el arreglo" << endl;
    }
}
int recorrer(int *arreglo){
    // Inicializar contador
    int contador = 0;
    for (int i=0; i<20; i++){
        // Si el numero se encuentra en la posicion
        if (arreglo[i] == '\0'){
            // El contador aumenta
            contador++;
        }
    }
    return contador;
}
// Función principal
int main (int argc, char *argv[]) {
    // Creacion de variables
    char* entrada = argv[1];
    int numero, numerobuscar, opcion, recor;
    int arreglo[20];
    char letra = entrada[0];
    Ordenamiento o = Ordenamiento();
    // Se rellena el arreglo con nulos
    for(int i=0; i<20; i++){
        arreglo[i] = '\0';
    }
    // Si el parametro de entrada es L
    if(entrada[0] == 'L'){
        cout << "-----------------------------" << endl;
        cout << "Prueba lineal" << endl;
        // Menu
        do {
            cout << "-----------------------------" << endl;
            cout << "(1) Ingresar numero" << endl;
            cout << "(2) Busqueda" << endl;
            cout << "(3) Salir" << endl;
            cout << "-----------------------------" << endl;
            cout << "Ingrese la opcion: ";
            
            // Se guarda la opcion 
            cin >> opcion;
            switch (opcion) {
            // Si la opcion es 1 pedira el ingreso del numero
            case 1:
                // Se llama a la funcion recorrer para usar el contador
                recor = recorrer(arreglo);
                // Si el contador es distinto de 0
                if (recor != 0){
                    cout << "Ingrese el numero: " << endl;
                    // Se guarda el numero ingresado
                    cin >> numero; 
                    // Se llama uncion hash
                    o.add_hash(arreglo, numero, letra);
                    // Se imprime el arreglo
                    for(int i=0; i<20; i++){
                        cout << "[" <<arreglo[i] << "]";
                    }
                    cout << endl;
                }
                else{
                    cout << "Lista llena" << endl;
                } 
                break;
                
            case 2:
                // Condicion busqueda de un numnero
                cout << "¿Que numero desea buscar?" << endl;
                // Se guarda la variable
                cin >> numerobuscar;
                // Se llama a la funcoin busqueda
                busqueda(arreglo, numerobuscar);
                break;
            } 
            // Si la opcion es 2, terminara el programa
        }while (opcion != 3);
    }
    // Si el parametro de entrada es C
    if(entrada[0] == 'C'){
        cout << "-----------------------------" << endl;
        cout << "Prueba cuadratica" << endl;
        // Menu
        do {
            cout << "-----------------------------" << endl;
            cout << "(1) Ingresar numero" << endl;
            cout << "(2) Busqueda" << endl;
            cout << "(3) Salir" << endl;
            cout << "-----------------------------" << endl;
            cout << "Ingrese la opcion: ";
            
            // Se guarda la opcion 
            cin >> opcion;
            
            switch (opcion) {
            // Si la opcion es 1 pedira el ingreso del numero
            case 1:
                // Se llama a la funcion recorrer para usar el contador
                recor = recorrer(arreglo);
                // Si el contador es distinto de 0
                if (recor != 0){
                    cout << "Ingrese el numero: " << endl;
                    // Se guarda el numero ingresado
                    cin >> numero; 
                    // Se llama uncion hash
                    o.add_hash(arreglo, numero, letra);
                    // Se imprime el arreglo
                    for(int i=0; i<20; i++){
                        cout << "[" <<arreglo[i] << "]";
                    }
                    cout << endl;
                }
                else{
                    cout << "Lista llena" << endl;
                } 
                break;
                
            case 2:
                // Condicion busqueda de un numnero
                cout << "¿Que numero desea buscar?" << endl;
                // Se guarda la variable
                cin >> numerobuscar;
                // Se llama a la funcoin busqueda
                busqueda(arreglo, numerobuscar);
                break;
            } 
            // Si la opcion es 2, terminara el programa
        }while (opcion != 3);
    }
    // Si el parametro de entrada es D
    if(entrada[0] == 'D'){
        cout << "-----------------------------" << endl;
        cout << "Doble direccion hush" << endl;
        // Menu
        do {
            cout << "-----------------------------" << endl;
            cout << "(1) Ingresar numero" << endl;
            cout << "(2) Busqueda" << endl;
            cout << "(3) Salir" << endl;
            cout << "-----------------------------" << endl;
            cout << "Ingrese la opcion: ";
            
            // Se guarda la opcion 
            cin >> opcion;
            
            switch (opcion) {
            // Si la opcion es 1 pedira el ingreso del numero
            case 1:
                // Se llama a la funcion recorrer para usar el contador
                recor = recorrer(arreglo);
                // Si el contador es distinto de 0
                if (recor != 0){
                    cout << "Ingrese el numero: " << endl;
                    // Se guarda el numero ingresado
                    cin >> numero; 
                    // Se llama uncion hash
                    o.add_hash(arreglo, numero, letra);
                    // Se imprime el arreglo
                    for(int i=0; i<20; i++){
                        cout << "[" <<arreglo[i] << "]";
                    }
                    cout << endl;
                }
                else{
                    cout << "Lista llena" << endl;
                } 
                break;
                
            case 2:
                // Condicion busqueda de un numnero
                cout << "¿Que numero desea buscar?" << endl;
                // Se guarda la variable
                cin >> numerobuscar;
                // Se llama a la funcoin busqueda
                busqueda(arreglo, numerobuscar);
                break;
            } 
            // Si la opcion es 2, terminara el programa
        }while (opcion != 3);
    }
    return 0;
}
