**Guia 9 Unidad 3**                                                                                            
Programa basado en metodos de busqueda                                                                          

**Pre-requisitos**                                                                                                                                    
Cplusplus                                                                                                                                              
Make                                                                                                                                      
Ubuntu                                                                                                                                                                                                                                                                                                                                                               
**Instalación**                                                                                                                                       
-Instalar Make si no esta en su computador.                                

Para instalar Make inicie la terminal y escriba lo siguiente:

sudo apt install make.                                                                                                                                                                                                                                                                                                         
**Problematica**                                                                                                                                       
Escribir un programa que pueda ingresar elementos en un arreglo y permita su busqueda, definir una funcion hash que distribuya los registros en el arreglo. solucionando las colisiones, Ademas se debe crear los metodos de reasignacion Prueba lineal, Prueba Cuadratica, Doble direccion hash y encadenamiento.

**Ejecutando**                                                                                                                                         
Para abrir el programa necesita ejecutar el archivo make desde la terminal, luego escribir en la terminal: ./programa L para iniciar el programa con el metodo de reasignacion prueba lineal, ./programa C para inicial el programa con el metodo de reasignacion prueba cuadratica y ./programa D para iniciar el programa con el metodo doble direccion hash.

Una vez iniciado el programa, este le mostrara un menu con 3 opciones, la opcion 1 le permitira el ingreso de numeros al arreglo y le mostrara en que posicion se ingreso, la opcion 2 le permitira cualquier numero en el arreglo y le dira en que posicion se encuentra y finalmente la opcion 3 le permitira salir del programa.

                                                                                            
**Construido con**                                                                                                                                    
C++                                                                                                                                      
                                                                                                                                         
Librerias:
- Iostream     


**Versionado**                                                                                                                                        
Version 1.1.2                                                                                                                                      

**Autores**                                                                                                                                                                                                                                                 
Rodrigo Valenzuela                                                                                                                                             
